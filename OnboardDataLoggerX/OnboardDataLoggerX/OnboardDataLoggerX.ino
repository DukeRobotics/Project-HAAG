/*
	* Duke Robotics
 * High Altitude Autonomous Glider
 *
 * Control and datalogging board
 *
 * On I2C bus:
 * DS1307, LPS331AP, LSM9DS0, SI1145
 *
 * external T/humidity sensor
 * DHT22, data pin is A3 on Arduino
 */

/* Should output over serial to BeagleBone as csv, raw data:
 * accel(x,y,z), gyro(r,p,q), pressure (24bit), uv, T, humidity
 *
 * Should accept over serial:
 * signal to release from balloon
 */

#include <Wire.h>
#include <SPI.h> // included for LSM9DS0
#include <SD.h>
#include <RTClib.h>
#include <LPS331.h>
#include <SFE_LSM9DS0.h>
#include <Adafruit_SI1145.h>
#include<Servo.h>
#include <Time.h>

#define LOG_INTERVAL  1000 // mills between entries
#define ECHO_TO_SERIAL   1 // echo data to serial port
#define WAIT_TO_START    0 // Wait for serial input in setup()
#define SYNC_INTERVAL 1000 // mills between calls to flush() - to write data to the card

// Pins, chipselect is for SD card
#define RELEASE_SERVO 14
#define NICHROME 6
const int chipSelect = 10;

// Servo
Servo Release;

// Real Time Clock, and last time of sync
RTC_DS1307 RTC;
uint32_t syncTime = 0;

// Log files
File logfile;
File resumeFile;

// Define LPS331AP pressure sensor
LPS331 lps331;

// Define accelerometer/gyro
#define ADR_GYRO	0x1E
#define ADR_XM		0x6B
LSM9DS0 dof(MODE_I2C, ADR_GYRO, ADR_XM);

// define SI1145 UV sensor
Adafruit_SI1145 uv = Adafruit_SI1145();

// Height at which we release
#define RELEASE_HEIGHT 18200
#define HEIGHT_THRESHOLD 60

#define TIME_TO_LAUNCH 30L
//#define TIME_TO_LAUNCH = 2400L

// Height counter
int heightCounter = 0;

// Should we continue trying to write to SD?
boolean haveSD = false;
boolean haveSerial = true;

boolean haveAccel = false;
boolean haveUV = false;
boolean havePressure = false;

boolean firstLoop = false;

// Things to log
float temp, pressure, altitude, UVindex, ax, ay, az, gx, gy, gz, mx, my, mz;

/*
    Record message to two different mediums
 */
void record(String message) {
    if (haveSD) {
        logfile.print(message);
    }
    if (haveSerial) {
        Serial.print(message);
    }
}
void recordln(String message) {
    if (haveSD) {
        logfile.println(message);
    }
    if (haveSerial) {
        Serial.println(message);
    }
}
void recordln() {
    if (haveSD) {
        logfile.println();
    }
    if (haveSerial) {
        Serial.println();
    }
}


/*
    Current time as a unix timestamp
 */
time_t getCurrentTime() {
    DateTime currentTime = RTC.now();
    return currentTime.unixtime();
}


/*
    Release the plane
 */
void release() {
    
    // Write to the launch file
    File launchFile = SD.open("HAVE_LAUNCHED.txt", FILE_WRITE);
    launchFile.println(getCurrentTime());
    launchFile.close();
    
    recordln("Release activated");
    logfile.flush();
    Release.attach(RELEASE_SERVO);
    for (int i=0; i< 180; i++) {
        Release.write(i);
        delay(10);
    }
    
    recordln("Nichrome activated");
    logfile.flush();
    delay(10);
    digitalWrite(NICHROME, HIGH);
    delay(2000);
    digitalWrite(NICHROME, LOW);
    recordln("Nichrome deactivated");
    logfile.flush();
    
}


/*
 Unix timestamp of when the program was compiled.
 Notice: only accurate to around 5 seconds, or the
 time it takes to upload the program itself.
 */
time_t getCompileTime() {
    const char* date = __DATE__;
    const char* time = __TIME__;
    
    char s_month[5];
    int month, day, year, hours, minutes, seconds;
    TimeElements elements = {0,0,0,0,0,0};
    static const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";
    
    sscanf(date, "%s %d %d", s_month, &day, &year);
    sscanf(time, "%d:%d:%d", &hours, &minutes, &seconds);
    
    month = (strstr(month_names, s_month)-month_names)/3;
    
    DateTime* compile = new DateTime(year, month + 1, day, hours, minutes, seconds);
    return compile[0].unixtime();
}

/*
 The existance of the file says we have launched
 */
boolean hasLaunched() {
    return (SD.exists("HAVE_LAUNCHED.txt"));
}

void setup(void){
    
    // Start serial
    Serial.begin(9600);
    Serial.println("Duke Robotics HAAG control board");
    
    delay(1000);
    
    //Nichrome setup
    pinMode(NICHROME, OUTPUT);
    digitalWrite(NICHROME, LOW);
    
    // Start SD
    Serial.println("Initializing SD card...");
    pinMode(10, OUTPUT);
    
    delay(1000);
    
    if (!SD.begin(chipSelect)) {
        Serial.println("Card failed, or not present");
        haveSD = false;
    } else {
        Serial.println("Card initialized.");
        haveSD = true;
    }
    
    delay(1000);
    
    
    // Find the next good file name for logging
    char filename[] = "LOGGER00.CSV";
    if (haveSD) {
        for (uint8_t i = 0; i < 100; i++) {
            filename[6] = i/10 + '0';
            filename[7] = i%10 + '0';
            if (!SD.exists(filename)) {
                Serial.print("Opening ");
                Serial.print(filename);
                Serial.println(" for writing");
                logfile = SD.open(filename, FILE_WRITE);
                break;
            }
        }
    }
    
    delay(1000);
    
    // Do we have a place to log to?
    if (haveSD && logfile) {
        Serial.print("Logging to: ");
        Serial.println(filename);
    } else {
        Serial.println("Failed to open log for writing!");
        haveSD = false;
    }
    
    delay(1000);
    
    // setup RTC
    Wire.begin();
    if (!RTC.begin()) {
        Serial.println("RTC failed!");
    } else {
        Serial.println("RTC success");
    }
    
    delay(1000);
    
    // Setup LPS331AP
    if (!lps331.init(0b1011101)) {
        Serial.println("LPS331 (pressure) failed");
        havePressure = false;
    } else {
        Serial.println("LPS331 (pressure) success");
        havePressure = true;
    }
    lps331.enableDefault();
    
    delay(1000);
    
    
    // Setup LSM9DS0
    uint16_t status = dof.begin();
    if (status != 0x49D4) {
        Serial.println("Accelerometer setup failed");
        haveAccel = false;
    } else {
        Serial.println("Accelerometer setup success");
        haveAccel = true;
    }
    
    // Setup SI1145
    if (!uv.begin()) {
        Serial.println("SI1145 (UV) failed");
        haveUV = false;
    } else {
        Serial.println("SI1145 (UV) success");
        haveUV = true;
    }
    
    
}

void loop(void){
    
    // ax, ay, az : accelerometer values
    // gx, gy, gz : gyroscope values
    // mx, my, mz : magnemometer values
    if (!firstLoop) {
        firstLoop = true;
    
        if (haveSD) {
            logfile.println("millis, time, temp (C), pressure (mb), altitude (m), uv, ax, ay, az, gx, gy, gz, mx, my, mz");
        }
        if (haveSerial) {
            Serial.println("millis, time, temp (C), pressure (mb), altitude (m), uv, ax, ay, az, gx, gy, gz, mx, my, mz");
        }
        
    }
    delay(1000);
    
    // Delay for the amount of time we want between readings
    delay((LOG_INTERVAL -1) - (millis() % LOG_INTERVAL));
    DateTime now;
    
    // Log milliseconds since starting
    uint32_t m = millis();
    if (haveSD) {
        logfile.print(m);
        logfile.print(", ");
    }
    if (haveSerial) {
        Serial.print("millis: ");
        Serial.print(m);
        Serial.print("\t");
    }
    
    delay(1000);
    
    // Fetch the time
    now = RTC.now();
    
    // Record the time
    if (haveSerial) {
        Serial.print(now.year(), DEC);
        Serial.print("/");
        Serial.print(now.month(), DEC);
        Serial.print("/");
        Serial.print(now.day(), DEC);
        Serial.print(" ");
        Serial.print(now.hour(), DEC);
        Serial.print(":");
        Serial.print(now.minute(), DEC);
        Serial.print(":");
        Serial.print(now.second(), DEC);
        Serial.print(", ");
    }
    if (haveSD) {
        logfile.print(now.year(), DEC);
        logfile.print("/");
        logfile.print(now.month(), DEC);
        logfile.print("/");
        logfile.print(now.day(), DEC);
        logfile.print(" ");
        logfile.print(now.hour(), DEC);
        logfile.print(":");
        logfile.print(now.minute(), DEC);
        logfile.print(":");
        logfile.print(now.second(), DEC);
        logfile.print(", ");
    }
    
    delay(1000);
    
    
    // Get the temperature, pressure, and altitude
    if (havePressure) {
        Serial.println();
        temp = lps331.readTemperatureC();
        pressure = lps331.readPressureMillibars();
        altitude = lps331.pressureToAltitudeMeters(pressure);
    } else {
        temp = 0.0;
        pressure = 0.0;
        altitude = 0.0;
    }
    
    // See if it's time to release via the barometer
    /*
     if (altitude > RELEASE_HEIGHT && heightCounter != -1) {
     heightCounter++;
     } else {
     heightCounter = 0;
     }
     if (heightCounter > HEIGHT_THRESHOLD) {
     if (hasLaunched()) {
     heightCounter = -1;
     } else {
     recordln("Starting release via barometer...");
     logfile.flush();
     release();
     heightCounter = -1;
     }
     }
     */
    
    // See if it's time to release via the timer
    if (millis() > 20000L) {
        if (getCurrentTime() - getCompileTime() > TIME_TO_LAUNCH) {
            if (!haveSD || !hasLaunched()) {
                
                Serial.print(":");
                /*
                Serial.print("current time is ");
                
                Serial.println(getCurrentTime());
                Serial.print("compiled time is ");
                Serial.println(getCompileTime());
                 */
                
                recordln("Starting release via timer...");
                
                logfile.flush();
                release();
                heightCounter = -1;
            }
        }
    }
    
    if (haveSD) {
        logfile.print(temp);
        logfile.print(", ");
        logfile.print(pressure);
        logfile.print(", ");
        logfile.print(altitude);
        logfile.print(", ");
    }
    if (haveSerial) {
        Serial.print("Temp (C): ");
        Serial.print(temp, 1);
        Serial.print("\t");
        Serial.print("Press (mb): ");
        Serial.print(pressure, 2);
        Serial.print("\t");
        Serial.print("Alt (m): ");
        Serial.print(altitude, 1);
        Serial.print("\t");
    }
    
    
    // Get the UV light index by dividing by 100
    if (haveUV) {
        UVindex = uv.readUV();
        UVindex /= 100.0;
    } else {
        UVindex = 0.0;
    }
    
    if (haveSD) {
        logfile.print(UVindex);
    }
    if (haveSerial) {
        Serial.println(UVindex);
    }
    
    // Accelerometer, calculated to g's from raw ADC value
    if (haveAccel) {
        dof.readAccel();
        ax = dof.calcAccel(dof.ax);
        ay = dof.calcAccel(dof.ay);
        az = dof.calcAccel(dof.az);
    } else {
        ax = 0.0;
        ay = 0.0;
        az = 0.0;
    }
    
    // Gyroscope, calculated to DPS value from raw ADC value
    if (haveAccel) {
        dof.readGyro();
        gx = dof.calcGyro(dof.gx);
        gy = dof.calcGyro(dof.gy);
        gz = dof.calcGyro(dof.gz);
    } else {
        gx = 0.0;
        gy = 0.0;
        gz = 0.0;
    }
    
    // Magnemometer, calculated to Gauss from raw ADC value
    if (haveAccel) {
        dof.readMag();
        float mx = dof.calcMag(dof.mx);
        float my = dof.calcMag(dof.my);
        float mz = dof.calcMag(dof.mz);
    } else {
        mx = 0.0;
        my = 0.0;
        mz = 0.0;
    }

    // Record values
    if (haveSD) {
        logfile.print(ax, 2);
        logfile.print(", ");
        logfile.print(ay, 2);
        logfile.print(", ");
        logfile.print(az, 2);
        logfile.print(", ");
        logfile.print(gx, 2);
        logfile.print(", ");
        logfile.print(gy, 2);
        logfile.print(", ");
        logfile.print(gz, 2);
        logfile.print(", ");
        logfile.print(mx, 2);
        logfile.print(", ");
        logfile.print(my, 2);
        logfile.print(", ");
        logfile.print(mz, 2);
        logfile.print(", ");
    }
    if (haveSerial) {
        Serial.println(ax, 2);
        Serial.println(", ");
        Serial.println(ay, 2);
        Serial.println(", ");
        Serial.println(az, 2);
        Serial.println(", ");
        Serial.println(gx, 2);
        Serial.println(", ");
        Serial.println(gy, 2);
        Serial.println(", ");
        Serial.println(gz, 2);
        Serial.println(", ");
        Serial.println(mx, 2);
        Serial.println(", ");
        Serial.println(my, 2);
        Serial.println(", ");
        Serial.println(mz, 2);
        Serial.println(", ");
    }
    
    recordln();
    
    if ((millis() - syncTime) < SYNC_INTERVAL) return;
    syncTime = millis();
    logfile.flush();
}

