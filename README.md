# Project H.A.A.G.

We kicked off 2015 with an ambitious project: to launch an autonomous glider from a high altitude weather balloon at 60,000 feet. Project H.A.A.G. (High Altitude Autonomous Glider), posed a series of new challenges for the club, including learning about new hardware, software, and algorithm design.

This project brought together an Arduino based flight controller, an aerogel insulated atmospheric data logger, redundant payload release mechanisms, and live telemetry up to a mile away. We learned a lot from this project that we will be able to apply to future autonomous robotics projects, like project Phorcys.
