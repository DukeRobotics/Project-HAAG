/*
 * Duke Robotics
 * High Altitude Autonomous Glider
 *
 * Control and datalogging board
 *
 * Writte by Austin McKee
 */




#include <Wire.h>
#include <SD.h>
#include <RTClib.h>
#include <Servo.h>
#include <Time.h>

// How much we delay every cycle
#define INTERVAL 4000

// After how many seconds do we release
#define RELEASE_TIME 240

// How long do we keep the Nichrome wire on
#define NICHROME_TIME 2000

// Pins, chipsSelect is for SD card
#define RELEASE_SERVO 14
#define NICHROME 6
const int chipSelect = 10;

// Servo
Servo Release;

// To what do we output
boolean writeSD = false;
boolean writeSerial = false;

// The Real Time Clock object
RTC_DS1307 RTC;

// Keep this cached
time_t compiledTime;

// Storing launch data in nvram on the RTC
#define LAUNCH_STATUS_ADDRESS 1
#define LAUNCHED_TRUE  0x01
#define LAUNCHED_FALSE 0x00

/*
 Current time as a unix timestamp
 */
time_t getCurrentTime() {
    DateTime currentTime = RTC.now();
    return currentTime.unixtime();
}

/*
 Unix timestamp of when the program was compiled.
 Notice: only accurate to around 5 seconds, or the
 time it takes to upload the program itself.
 */
time_t getCompileTime() {
    const char* date = __DATE__;
    const char* time = __TIME__;
    
    char s_month[5];
    int month, day, year, hours, minutes, seconds;
    TimeElements elements = {0,0,0,0,0,0};
    static const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";
    
    sscanf(date, "%s %d %d", s_month, &day, &year);
    sscanf(time, "%d:%d:%d", &hours, &minutes, &seconds);
    
    month = (strstr(month_names, s_month)-month_names)/3;
    
    DateTime* compile = new DateTime(year, month + 1, day, hours, minutes, seconds);
    return compile[0].unixtime();
}

boolean hasLaunched() {
    if (RTC.readnvram(LAUNCH_STATUS_ADDRESS) == LAUNCHED_TRUE) {
        return true;
    } else {
        return false;
    }
}
void launch() {
    RTC.writenvram(LAUNCH_STATUS_ADDRESS, LAUNCHED_TRUE);
}
void resetLaunch() {
    RTC.writenvram(LAUNCH_STATUS_ADDRESS, LAUNCHED_FALSE);
}

void release() {
    
    // Make sure we don't try to do this again
    launch();
    
    // Activate the mechanical release
    Serial.println("Release activated");
    Release.attach(RELEASE_SERVO);
    for (int i=0; i< 180; i++) {
        Release.write(i);
        delay(10);
    }
    
    // Activate the nichrome wire
    Serial.println("Nichrome activated");
    digitalWrite(NICHROME, HIGH);
    delay(NICHROME_TIME);
    digitalWrite(NICHROME, LOW);
    Serial.println("Nichrome deactivated");
}

void setup(void) {
    
    // Begin Serial
    Serial.begin(9600);
    Serial.println("Duke Robotics HAAG control board");
    
    // Nichrome setup
    pinMode(NICHROME, OUTPUT);
    digitalWrite(NICHROME, LOW);
    
    // Start Real Time Clock
    Wire.begin();
    if (!RTC.begin()) {
        Serial.println("RTC failed!");
    } else {
        Serial.println("RTC success");
    }
    
    // When was this program compiled
    compiledTime = getCompileTime();
}
void loop(void) {
    
    // Time since we compiled this program
    int timeDifference = (getCurrentTime() - compiledTime);
    
    // We are within 30 seconds of compile, reset launch status
    if (timeDifference < 30) {
        resetLaunch();
    }
    
    // Is it time to release?
    if (timeDifference > RELEASE_TIME && !hasLaunched()) {
        release();
    }
    
    // Give a status update
    Serial.print("Seconds since compile: ");
    Serial.println(timeDifference);
    delay(INTERVAL);
}















