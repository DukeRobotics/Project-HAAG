/*
	* Duke Robotics
 	* High Altitude Autonomous Glider
 	* 
 	* Control and datalogging board
 	* 
 	* On I2C bus:
 	* DS1307, LPS331AP, LSM9DS0, SI1145
 	* 
 	* external T/humidity sensor
 	* DHT22, data pin is A3 on Arduino
 	*/

/* Should output over serial to BeagleBone as csv, raw data:
 	* accel(x,y,z), gyro(r,p,q), pressure (24bit), uv, T, humidity
 	* 
 	* Should accept over serial:
 	* signal to release from balloon
 	*/
 
//One more test
#include <Wire.h>
#include <SPI.h> // included for LSM9DS0
#include <SD.h>
#include <RTClib.h>
#include <LPS331.h>
#include <SFE_LSM9DS0.h>
#include <Adafruit_SI1145.h>
#include<Servo.h>

#define LOG_INTERVAL  1000 // mills between entries
#define ECHO_TO_SERIAL   1 // echo data to serial port
#define WAIT_TO_START    0 // Wait for serial input in setup()
#define SYNC_INTERVAL 1000 // mills between calls to flush() - to write data to the card


#define RELEASE_SERVO 14
#define NICHROME 6
Servo Release;

RTC_DS1307 RTC; // define the Real Time Clock object
uint32_t syncTime = 0; // time of last sync()

// pin 10 is CS for SD card
const int chipSelect = 10;
File logfile;
File resumeFile;

// define LPS331AP pressure sensor
LPS331 lps331;  

// define accelerometer/gyro
#define ADR_GYRO	0x1E
#define ADR_XM		0x6B
LSM9DS0 dof(MODE_I2C, ADR_GYRO, ADR_XM);

// define SI1145 UV sensor
Adafruit_SI1145 uv = Adafruit_SI1145();

// Height at which we release
#define RELEASE_HEIGHT 18200
#define HEIGHT_THRESHOLD 60

// Height counter
int heightCounter = 0;

void error(char *str) {
  Serial.print("error: ");
  Serial.println(str);

  // red LED indicates error

    while(1);
}

/*
  Record message to two different mediums
*/
void record(String message) {
  logfile.print(message);
  Serial.print(message);
}

/*
  Same as above, but with a new line
*/
void recordln(String message) {
  logfile.println(message);
  Serial.println(message);
}
void recordln() {
  logfile.println();
  Serial.println();  
}

void setup(void){
	Serial.begin(9600);
	Serial.println("DR HAAG control board");
	
	Serial.print("Initializing SD card...");
	pinMode(10, OUTPUT);

        //Nichrome setup
        pinMode(NICHROME, OUTPUT);
        digitalWrite(NICHROME, LOW);
	
	if (!SD.begin(chipSelect)) {
		Serial.println("Card failed, or not present");
		// don't do anything more:
		return;
	}
	Serial.println("Card initialized.");
	
	char filename[] = "LOGGER00.CSV";
	for (uint8_t i = 0; i < 100; i++) {
		filename[6] = i/10 + '0';
		filename[7] = i%10 + '0';
		if (! SD.exists(filename)) {
			// only open a new file if it doesn't exist
			logfile = SD.open(filename, FILE_WRITE); 
			break;  // leave the loop!
		}
	}

	if (!logfile) {
		error("couldnt create file");
	}

  // We've now have a place to log to
  Serial.print("Logging to: ");
  Serial.println(filename);

  // setup RTC
  Wire.begin();  
  if (!RTC.begin()) {
    //recordln("RTC failed!");
    Serial.println("RTC failed!");
  } else {
    Serial.println("RTC success");
    //recordln("RTC success");  
  }
  Serial.println("asdf");
  
  // Setup LPS331AP
  if (!lps331.init(0b1011101)) {
    Serial.println("LPS331 (pressure) failed");
    //recordln("LPS331 (pressure) failed");
  } else {
    Serial.println("LPS331 (pressure) success");
    //recordln("LPS331 (pressure) success");
  }
  lps331.enableDefault();




  // Setup LSM9DS0
  uint16_t status = dof.begin();
  if (status != 0x49D4) {
    Serial.println("Accelerometer setup failed");
    //recordln("Accelerometer setup failed");
  } else {
    Serial.println("Accelerometer setup success");
    //recordln("Accelerometer setup success"); 
  }

  // Setup SI1145
  if (!uv.begin()) {
    Serial.println("SI1145 (UV) failed");
    //recordln("SI1145 (UV) failed");
  } else {
    Serial.println("SI1145 (UV) ");
    //recordln("SI1145 (UV) failed");
  }
  
  // ax, ay, az : accelerometer values
  // gx, gy, gz : gyroscope values
  // mx, my, mz : magnemometer values
  recordln("millis, time, temp (C), pressure (mb), altitude (m), uv, ax, ay, az, gx, gy, gz, mx, my, mz");
}

void loop(void){

  DateTime now;
  // delay for the amount of time we want between readings
  delay((LOG_INTERVAL -1) - (millis() % LOG_INTERVAL));


  // log milliseconds since starting
  uint32_t m = millis();
  logfile.print(m);
  logfile.print(", ");    
#if ECHO_TO_SERIAL
  Serial.print("millis: ");
  Serial.print(m);
  Serial.print("\t");
#endif

  // Fetch the time
  now = RTC.now();
  
  // Record the time
  Serial.println(now.year(), DEC);
  Serial.println("/");
  Serial.println(now.month(), DEC);
  Serial.println("/");
  Serial.println(now.day(), DEC);
  Serial.println(" ");
  Serial.println(now.hour(), DEC);
  Serial.println(":");
  Serial.println(now.minute(), DEC);
  Serial.println(":");
  Serial.println(now.second(), DEC);
  Serial.println(", ");
  
  logfile.print(now.year(), DEC);
  logfile.print("/");
  logfile.print(now.month(), DEC);
  logfile.print("/");
  logfile.print(now.day(), DEC);
  logfile.print(" ");
  logfile.print(now.hour(), DEC);
  logfile.print(":");
  logfile.print(now.minute(), DEC);
  logfile.print(":");
  logfile.print(now.second(), DEC);
  logfile.print(", ");


  // Get the temperature, pressure, and altitude

  float temp = lps331.readTemperatureC();
  float pressure = lps331.readPressureMillibars();
  float altitude = lps331.pressureToAltitudeMeters(pressure);
  
  // See if it's time to release via the barometer
  if (altitude > RELEASE_HEIGHT && heightCounter != -1) {
     heightCounter++; 
  } else {
     heightCounter = 0; 
  }
  if (heightCounter > HEIGHT_THRESHOLD) {
    if (hasLaunched()) {
      heightCounter = -1;
    } else {
      recordln("Starting release via barometer...");
      logfile.flush();
      release();  
      heightCounter = -1;
    }
  }
  
  // See if it's time to release via the timer
  if (millis() > 20000L) {
    if (getCurrentTime() - getCompileTime() > 2400 && !hasLaunched()) {
      recordln("Starting release via timer...");
      logfile.flush();
      release();  
      heightCounter = -1;
    }
  }

  logfile.print(temp); 
  logfile.print(", ");
  logfile.print(pressure); 
  logfile.print(", ");
  logfile.print(altitude); 
  logfile.print(", ");

#if ECHO_TO_SERIAL
  Serial.print("Temp (C): "); 
  Serial.print(temp, 1); 
  Serial.print("\t");
  Serial.print("Press (mb): "); 
  Serial.print(pressure, 2); 
  Serial.print("\t");
  Serial.print("Alt (m): "); 
  Serial.print(altitude, 1); 
  Serial.print("\t");
#endif


  // Get the UV light index
  float UVindex = uv.readUV();
  
  // the index is multiplied by 100 so to get the
  // integer index, divide by 100!
  UVindex /= 100.0;
  
  logfile.print(UVindex);
  Serial.println(UVindex);

  // Get acceleration, gyroscope, and magnemometer readings

  // accelerometer, calculated to g's from raw ADC value
  dof.readAccel();
  float ax = dof.calcAccel(dof.ax);
  float ay = dof.calcAccel(dof.ay);
  float az = dof.calcAccel(dof.az);

  // gyroscope, calculated to DPS value from raw ADC value
  dof.readGyro();
  float gx = dof.calcGyro(dof.gx);
  float gy = dof.calcGyro(dof.gy);
  float gz = dof.calcGyro(dof.gz);

  // magnemometer, calculated to Gauss from raw ADC value
  dof.readMag();
  float mx = dof.calcMag(dof.mx);
  float my = dof.calcMag(dof.my);
  float mz = dof.calcMag(dof.mz);

  // record values
  logfile.print(ax, 2); 
  logfile.print(", ");
  logfile.print(ay, 2); 
  logfile.print(", ");
  logfile.print(az, 2); 
  logfile.print(", ");
  logfile.print(gx, 2); 
  logfile.print(", ");
  logfile.print(gy, 2); 
  logfile.print(", ");
  logfile.print(gz, 2); 
  logfile.print(", ");
  logfile.print(mx, 2); 
  logfile.print(", ");
  logfile.print(my, 2); 
  logfile.print(", ");
  logfile.print(mz, 2); 
  logfile.print(", ");
  
  Serial.println(ax, 2); 
  Serial.println(", ");
  Serial.println(ay, 2); 
  Serial.println(", ");
  Serial.println(az, 2); 
  Serial.println(", ");
  Serial.println(gx, 2); 
  Serial.println(", ");
  Serial.println(gy, 2); 
  Serial.println(", ");
  Serial.println(gz, 2); 
  Serial.println(", ");
  Serial.println(mx, 2); 
  Serial.println(", ");
  Serial.println(my, 2); 
  Serial.println(", ");
  Serial.println(mz, 2); 
  Serial.println(", ");

  recordln();

  if ((millis() - syncTime) < SYNC_INTERVAL) return;
  syncTime = millis();
  logfile.flush();
}

/*
  Release the plane
*/
void release() {
  
  // Write to the launch file
  File launchFile = SD.open("HAVE_LAUNCHED.txt", FILE_WRITE);
  launchFile.println(getCurrentTime());
  launchFile.close();
  
  recordln("Release activated");
  logfile.flush();
  Release.attach(RELEASE_SERVO);
  for (int i=0; i< 180; i++) {
    Release.write(i);
    delay(10);
  }
  
  recordln("Nichrome activated"); 
  logfile.flush();
  delay(10);
  digitalWrite(NICHROME, HIGH);
  delay(2000);
  digitalWrite(NICHROME, LOW);
  recordln("Nichrome deactivated");
  logfile.flush();
 
}

/*
  Current time as a unix ti
*/
time_t getCurrentTime() {
  DateTime currentTime = RTC.now();
  return currentTime.unixtime();
}

/*
  Unix timestamp of when the program was compiled.
  Notice: only accurate to around 5 seconds, or the
  time it takes to upload the program itself.
*/
time_t getCompileTime() {
  const char* date = __DATE__;
  const char* time = __TIME__;
  
  char s_month[5];
  int month, day, year, hours, minutes, seconds;
  TimeElements elements = {0,0,0,0,0,0};
  static const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";

  sscanf(date, "%s %d %d", s_month, &day, &year);
  sscanf(time, "%d:%d:%d", &hours, &minutes, &seconds);

  month = (strstr(month_names, s_month)-month_names)/3;
  
  DateTime* compile = new DateTime(year, month + 1, day, hours, minutes, seconds);
  return compile[0].unixtime();  
}

/*
  The existance of the file says we have launched
*/
boolean hasLaunched() {
  return (SD.exists("HAVE_LAUNCHED.txt"));
}

