Data Logging and Release Code
=================

This code runs on the Arduino in the "mothership" box of the flight train.
It logs pressure, temperature, UV Index, humidity, and accelerometer data
to the SD card on the board. It also controls the nichrome wire and mechanical
release mechanism.