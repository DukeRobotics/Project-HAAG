# This is the main script that runs on our BeagleBone Black
# It's purpose is to control the systems that execute our mission task
# Duke-robotics.com
# Features:
#   Communication:
#       log all important data serial incoming (CSV: accel (xyz 3 16-bit figures xyz), gyro (3 16-bit figures xyz), barometer(pressure in raw chip value lps331ap 1 24-bit figure))
#       log everything else beyond that too
#       send drop command over serial: ("dropnow") every 1 s for 60 s when it's time
#   Navigation:
# We are currently ascending.
#Lock the servos in place
#don’t try to make attitude adjustments
#Set throttle to zero because we don’t have a motor
#
#while phase = 0
#	#Ascending
#	Move the servos gently every 10s to prevent freezing
#	
#	if (cut_wire)
#		phase = 1
#
#get (x,y) offset vector from initial launch (x,y) 
#this gives average wind direction on ascent
#
#get current altitude #max altitude
#
# the first leg of the descent!
#while (phase = 1)
#heading = -avg_wind_dir vector
#fly towards GPS waypoint in direction of heading
#
#if (altitude = max altitude/2)
#	phase = 2
#	# this is the second leg, we should turn around!
#
# the second leg of the descent!
#while (phase = 2)
#	fly towards GPS waypoint of landing site 
#the largest flattest place near Durham, TBD       

#connections
# UART	RX	TX	CTS	RTS	Device
# UART1	P9_26	P9_24	P9_20	P9_19	/dev/ttyO1

# dependencies
# pip install pyserial
import os.path
import Adafruit_BBIO.UART as UART

#state
ourLog = "/usr/data/controller/log.txt"

# initialization
UART.setup("UART1")
ser = serial.Serial(port = "/dev/ttyO1", baudrate=9600)
ser.close()
ser.open()
if ser.isOpen():
    print "Serial to datalogger is open!"
    ser.write("Controller: Serial connection to datalogger connected.")


# call this often
def logData():
    if (os.path.isfile(ourLog)):
        # our log file already exists
        fo = open(ourLog, "a+")
        # Get serial data buffer
        inp = ser.read();
        fo.write(...
        
        fo.close()
    else:
        fo = open(ourLog, "w+")


        fo.close()


    

